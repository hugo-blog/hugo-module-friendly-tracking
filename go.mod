module git.ingo-struck.com/hugo-blog/hugo-module-friendly-metrics

go 1.19

require git.ingo-struck.com/hugo-blog/hugo-module-i18n v0.2.3 // indirect

require git.ingo-struck.com/hugo-blog/hugo-module-secure-resource v0.11.1 // indirect
